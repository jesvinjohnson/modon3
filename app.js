var express = require('express');
var path = require('path');
var axios = require('axios');
var fs = require('fs');

var app = express();

var server = require('http').createServer(app);
var io = require('socket.io')(server);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var http = require('http');
var server = http.createServer(app);
server.setTimeout(10*60*1000);

var io = io.listen(server);
//above defined dependencies are required to run this project.



//current working port for the project. // port=3000
server.listen(3000, function () {
  console.log('Listening on port 3000');
  console.log('Cmd - npm start ');
});


//To initialize the sockets for the project,
//for exchanging data between ipad and main screen.
io.on('connection', function(client) {
    console.log('Client connected...');

    client.on('join', function(data) {
        console.log(data);
    });
    client.on('broad', function(data) {
        console.log(data);
    });

    client.on('messages', function(data) {
        client.emit('broad', data);
        client.broadcast.emit('broad',data);
    });
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

//const PORT = 3000;

var router = express.Router();




// -- API calls --

//to open and access the main screen 
router.get('/', function (req, res) {
  	res.render('index');
});

//to access the inner screen 
router.get('/pages/section2', function (req, res) {
  res.render('pages/section2');
});

//to access the inner screen 
router.get('/pages/section3', function (req, res) {
  res.render('pages/section3');
});

//to access the inner screen 
router.get('/pages/section4', function (req, res) {
  res.render('pages/section4');
});

//to access the inner screen 
router.get('/pages/section5', function (req, res) {
  res.render('pages/section5');
});

//to access the inner screen 
router.get('/pages/section6', function (req, res) {
	var id = req.query.id;

	fs.readdir('./public/images/gallery/'+id, (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
    
		axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
	        IndustrialCityID: id
	    }).then(function (response) {
	    	res.render('pages/section6', {data : response.data, cityid: id, resd:result });

	    }).catch(function (error) {

	        console.log('error');
	        console.log(error);
	    });
    });

});

//to access the inner screen 
router.get('/pages/section7', function (req, res) {

	fs.readdir('./public/images/techzone/riyadhTech', (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
		res.render('pages/section7', { resd:result });
 	});
  
});

//to access the inner screen 
router.get('/pages/section8', function (req, res) {

	fs.readdir('./public/images/techzone/wadi', (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
		res.render('pages/section8', { resd:result });
 	});
});

//to access the inner screen 
router.get('/pages/section9', function (req, res) {

	fs.readdir('./public/images/techzone/riyadhDigital', (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
		res.render('pages/section9', { resd:result });
 	});
});

//to access the inner screen 
router.get('/pages/section10', function (req, res) {
	res.render('pages/section10');
});


// ---IPAD section
//to access the inner screen 
router.get('/screen', function (req, res) {
  	res.render('index-ipad');
});

//to access the inner screen 
router.get('/pages-ipad/section2', function (req, res) {
	io.emit('broadML','mainlogo');
  	res.render('pages-ipad/section2');
});

//to access the inner screen 
router.get('/pages-ipad/section3', function (req, res) {
	selLang = req.query.lang;
	io.emit('broadML',selLang);
  	res.render('pages-ipad/section3');
});

//to access the inner screen 
router.get('/pages-ipad/section4', function (req, res) {
	btn = req.query.btn;

	io.emit('broadML',btn);
  	res.render('pages-ipad/section4');
});

//to access the video functionality on main page
router.get('/videobtn', function (req, res) {
	playbtnid = req.query.id;
	io.emit('broadML',playbtnid);
	return res.json({ data : 'success'});
});

//to access the video functionality on main page
router.post('/videobutton', function (req, res) {
	io.emit('broadML','videobutton');
	return res.json({ data : 'success'});
});

//to access the video functionality on inner page
router.get('/videobutton2', function (req, res) {
	io.emit('broadML','videobutton2');
	return res.json({ data : 'success'});
});

//to access the animation part of the selected state.
router.post('/stateAnimate', function (req, res) {

	io.emit('broadML',req.body.stname);
	return res.json({ data : 'success'});
});

//to access the inner left slide menu screen 
router.get('/leftMenuSlide', function (req, res) {
	type = req.query.type;
	io.emit('broadML',type);
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/check1', function (req, res) {
	io.emit('broadML','check1');
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/check2', function (req, res) {
	io.emit('broadML','check2');
	return res.json({ data : 'success'});
});



//to access the inner screen 
router.get('/check3', function (req, res) {
	io.emit('broadML','check3');
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/uncheck1', function (req, res) {
	io.emit('broadML','uncheck1');
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/uncheck2', function (req, res) {
	io.emit('broadML','uncheck2');
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/uncheck3', function (req, res) {
	io.emit('broadML','uncheck3');
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/onLoad', function (req, res) {

io.emit('broadML','onLoad');
return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/mainReset', function (req, res) {

	io.emit('broadML','mainReset');
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/mainMap', function (req, res) {
	io.emit('broadML','mainMap');
	return res.json({ data : 'success'});
});


//to access the inner screen 
router.get('/pages-ipad/section5', function (req, res) {
	io.emit('broadML','section5');
  	res.render('pages-ipad/section5');
});


//to access the inner screen 
router.get('/pages-ipad/section6', function (req, res) {

	var type = req.query.type;
	var id = req.query.id;
	io.emit('broadML',type);
	var resd = [];

	fs.readdir('./public/images/gallery/'+id, (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
    
		axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
	        IndustrialCityID: id
	    }).then(function (response) {
	    	res.render('pages-ipad/section6', {data : response.data, cityid: id, resd:result });

	    }).catch(function (error) {

	        console.log('error');
	        console.log(error);
	    });
    });
});


//to access the inner screen 
router.get('/pages-ipad/section7', function (req, res) {
	var type = req.query.type;
	io.emit('broadML',type);

	fs.readdir('./public/images/techzone/riyadhTech', (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
		res.render('pages-ipad/section7', { resd:result });
 	});
  	
});


//to access the inner screen 
router.get('/pages-ipad/section8', function (req, res) {
	var type = req.query.type;
	io.emit('broadML',type);
	fs.readdir('./public/images/techzone/wadi', (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
		res.render('pages-ipad/section8', { resd:result });
 	});
 
});


//to access the inner screen 
router.get('/pages-ipad/section9', function (req, res) {
	var type = req.query.type;
	io.emit('broadML',type);
	fs.readdir('./public/images/techzone/riyadhDigital', (err, files) => {

		const result = files.filter(word => (word.includes('jpg') || word.includes('mp4')));
		res.render('pages-ipad/section9', { resd:result });
 	});
  
});


//to access the inner screen 
router.get('/pages-ipad/section10', function (req, res) {
	io.emit('broadML','section10');
  	res.render('pages-ipad/section10');
});


//to trigger the socket functions on the homepage. 
router.get('/fireSocket', function (req, res) {
	id = req.query.sel;
	io.emit('broadML',id);
	return res.json({ data : 'success'});

});


//to access the inner screen 
router.get('/section6LeftNav', function (req, res) {
	type = req.query.type;
	io.emit('broadML',type);
	return res.json({ data : 'success'});

});


//to access the inner left navigation screen of technology zone. 
router.get('/techZoneLeftNav', function (req, res) {
	type = req.query.type;
	io.emit('broadML',type);
	return res.json({ data : 'success'});

});


//to close popup modal on any screen
router.get('/closeModal', function (req, res) {
	io.emit('broadML','closeModal');
	return res.json({ data : 'success'});

});


//to access the inner screen and open video section
router.post('/openPopImgVideo', function (req, res) {
	datasrc = req.body.datasrc;
	io.emit('broadML',datasrc);
	return res.json({ data : 'success'});

});


//to access the inner screen and open video section
router.get('/sharePopupOpen', function (req, res) {

	datasrc = req.query.type;
	// console.log('app'+datasrc)
	io.emit('broadML',datasrc);
	return res.json({ data : 'success'});

});


//function to external api call of GIS server to get industrial info
//based on the given industrial id.
router.post('/fetchIndustrialData', function (req, res) {

	temp = req.body.industryData;
	lang = req.body.industryData;
	temp2 = temp.split('_');
	name = temp2[0];
	const id = temp2[1];
	axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
        IndustrialCityID: id
    }).then(function (response) {

    	if(response.data){

    		var out='<div class="image-area">'+
	            '<img src="images/gallery/'+id+'/1.jpg" class="card-img-top">'+
	            '<div class="text">'+
	                '<h4>'+response.data.IndustrialCityNameEn+'</h4>'+
	                '<p class="short-desc">'+response.data.CompetitiveAdvantageEn+'</p>'+
	            '</div>'+
        	'</div>'+
	        '<ul class="list-group list-group-flush">'+
	            '<li class="list-group-item"><span class="list-title">Established Date</span><span class="list-data">'+response.data.EstablishedDate+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">Total Area (m<sup>2</sup>)</span><span class="list-data">'+response.data.Area+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">Number of Factories</span><span class="list-data">'+response.data.Count+' factories</span></li>'+
	        '</ul>'+
	        '<div class="card-body">'+
	            '<a id="'+response.data.IndustrialCityID+'" href="#" class="card-link">more . . .</a>'+
			'</div>'

			var out2='<div class="image-area">'+
	            '<img src="images/gallery/'+id+'/1.jpg" class="card-img-top">'+
	            '<div class="text">'+
	                '<h4>'+response.data.IndustrialCityNameAr+'</h4>'+
	                '<p class="short-desc">'+response.data.CompetitiveAdvantageAr+'</p>'+
	            '</div>'+
        	'</div>'+
	        '<ul class="list-group list-group-flush">'+
	            '<li class="list-group-item"><span class="list-title">تاريخ الإنشاء</span><span class="list-data">'+response.data.EstablishedDate+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">المساحة الإجمالية  (م2)</span><span class="list-data">'+response.data.Area+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">عدد المصانع </span><span class="list-data">'+response.data.Count+' مصنع </span></li>'+
	        '</ul>'+
	        '<div class="card-body">'+
	            '<a id="'+response.data.IndustrialCityID+'" href="#" class="card-link">...المزيد</a>'+
	        '</div>'
    	}

    	return res.json({data : out, data2 : out2 });


    }).catch(function (error) {

        console.log('error');
    });
});

// -- EO API calls --


//function to send out acknowledgement emails to clients and modon admins
//based on the selected language(english or arabic)
router.post('/saveFormData', function (req, res) {

	var name = req.body.name;
	var company =  req.body.company;
	var jobtitle =  req.body.jobtitle;
	var email =  req.body.email;
	var cdata = req.body.selectedCities;
	
	if(req.body.selLang == 'english'){

		var out2= '';
		var i =1; cdata.forEach(function(vdata) {
			out2+= vdata.name+" : "+vdata.url+"<br>";
		i++; 
		}) 
		var msgbody = "Hi Team,<br><br>"+

			"Please find below the information of a user of the Interactive wall experience:<br>"+
			"Name: "+name+"<br>"+
			"Company: "+company+"<br>"+
			"Email: "+email+"<br><br>"+

			"Interested cities:<br>"+
				out2+
			"<br>Thank you";
	
	// ----------------------
		axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/SendEmailAdmin', {
		    emailid: 'interactivewall@modon.gov.sa',
		    subject: 'Modon Interactive wall experience',
		    messagebody: msgbody
		}).then(function (response) {
			if(response.data){

				console.log(response.data); 
			}
		}).catch(function (error) {
		    console.log(error);
		});
	// ----------------------

	// ----------------------
		var msgbody2= "Dear "+name+",<br><br>"+

		"Thank you for visiting and experiencing MODON interactive wall platform.<br><br>"+
		
		"Please find below the links to the MODON industrial cities that you are interested in. <br><br>"+
		
		out2+

		"<br><br>These links will help to answer any questions about the MODON industrial cities.<br><br>"+

		"For further information, kindly do not hesitate to contact us;<br><br>"+
		
		"Email: info@modon.gov.sa<br><br>"+
		
		"Unified No. 920000425<br><br>"+

		"Yours truly,<br><br>"+
		"MODON";

		axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/SendEmailClient', {
		    emailid: email,
		    subject: 'Modon Interactive wall experience',
		    messagebody: msgbody2
		}).then(function (response) {
			if(response.data){

				console.log(response.data); 
			}
		}).catch(function (error) {
		    console.log(error);
		});
	// ----------------------

	return res.json({ 'status': 200});
		
	}


	if(req.body.selLang == 'arabic'){

		var out2= '';
		var i =1; cdata.forEach(function(vdata) {
			out2+= vdata.name+" : "+vdata.url+"<br>";
		i++; 
		}) 
		var msgbody = "<div style='text-align: right; direction: rtl'>مرحبا بالفريق,<br><br>"+

			"تجدون أدناه معلومات مستخدم تجربة المنصة التفاعلية:<br>"+
			"الاسم: "+name+"<br>"+
			"الشركة: "+company+"<br>"+
			"البريد: "+email+"<br><br>"+

			"المدن المهتمة:<br>"+
				out2+
			"<br>شكرا لكم</div>";
	
	// ----------------------
		axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/SendEmailAdmin', {
		    emailid: 'interactivewall@modon.gov.sa', //'mabdul@modon.gov.sa',
		    subject: 'تجربة منصة مدن التفاعلية',
		    messagebody: msgbody
		}).then(function (response) {
			if(response.data){

				console.log(response.data); 
			}
		}).catch(function (error) {
		    console.log(error);
		});
	// ----------------------

	// ----------------------

		var msgbody2= "<div style='text-align: right; direction: rtl'>  عزيزي "+name+",<br><br>"+

		"شكراً لزيارتك 'مدن' وتجربتك للمنصة<br><br>"+
		
		"التفاعلية.<br><br>"+

		"وللمزيد من المعلومات عن المدينة<br><br>"+

		"الصناعية ذات الاهتمام يرجى زيارة الرابط<br><br>"+

		"أدناه:<br><br>"+

		out2+

		"<br>ولمزيد من الاستفسارات نسعد بتواصلك معنا عبر الهاتف الموحد: 920000425 أو عبر البريد الإلكتروني<br>"+
		"info@modon.gov.aa<br>";

		axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/SendEmailClient', {
		    emailid: email,
		    subject: 'تجربة منصة مدن التفاعلية',
		    messagebody: msgbody2
		}).then(function (response) {
			if(response.data){

				console.log(response.data); 
			}
		}).catch(function (error) {
		    console.log(error);
		});
	// ----------------------
		return res.json({ 'status': 200});
	}
});

app.use('/', router);

