
jQuery(document).ready(function() {

    /* ROUND BUTTON CLICK SOUND FUNCTION */

    const clickSound = new Audio("./audios/click.wav");

    jQuery("main").on("click",".btn_round", function(){
        clickSound.play()
    });

    //INIT and RESET VALUES

    function init(){

        //adding cities shortlisted
        APP.selectedCities = [];
        APP.addedCities = [];
        APP.makeCheckbox = function() {
            const checkboxHeader = `
                                <div class="checkbox">
                                    <label class="form-check-label" id="check0" for="check0"><span>Selected Cities</span></label>
                                </div>`;

            const checkbox = `
            ${APP.selectedCities.map(city =>
                            `
                            <div class="checkbox">
                                <input class="form-check-input" type="checkbox" checked value="${city.id}" id="check${city.no}">
                                <label class="form-check-label" for="check${city.id}"><span>${city.name}</span></label>
                            </div>
                            `
                            ).join('')}
                        `;

            $('.dropdown.dd_selected').first().after().html('').append(checkboxHeader+checkbox);
            $('.btn_round.briefcase .badge').html(APP.selectedCities.length);
          }

    }
    init();

    /* MENU SLIDING FUNCTION - SECTION 4,5 */

    jQuery("main").on("click","#showLeft", function(){
        var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
            body = document.getElementById( 'section4' );
            classie.toggle( this, 'active' );
            classie.toggle( menuLeft, 'cbp-spmenu-open' );
    });

    /* MENU SLIDING FUNCTION - SECTION 6 */

    jQuery("main").on("click","#showLeftPush", function(){
        var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
            body = document.getElementById( 'push' );
            classie.toggle( this, 'active' );
            classie.toggle( body, 'cbp-spmenu-push-toright' );
            classie.toggle( menuLeft, 'cbp-spmenu-open' );
    });

    /* ROUND BUTTON MENU TOGGLE FUNCTION */

    jQuery("main").on("click",".outer-btn", function(){
        jQuery('.inner-btn').fadeToggle();
    });

    /* POPUP OPEN FUNCTION - SECTION 5 */

    jQuery("main").on("click", function(e){
        jQuery('#section4 .map-popup').fadeOut();
        e.stopPropagation();
    });
    jQuery("main").on("click",".riyadh-img", function(e){
        e.stopPropagation();
        jQuery('#section4 .map-popup').fadeIn();
    });
    jQuery("main").on("click","#section4 .map-popup", function(e){
        e.stopPropagation();
    });

    /*VIDEO MODAL FUNCTIONS - SECTION 6 */

    // english

    jQuery("main").on("click",".english .btn_vdo", function(){
        jQuery('#mainvideoModal').modal();
    });

    jQuery("main").on("shown.bs.modal",".english #mainvideoModal",function(){
        var vid = document.getElementById("mainvideo");
        vid.play();
    });

    jQuery("main").on("hide.bs.modal",".english #mainvideoModal",function(){
        var vid = document.getElementById("mainvideo");
        vid.load();
    });

    // arabic

    jQuery("main").on("click",".arabic .btn_vdo", function(){
        jQuery('#mainvideoModalAra').modal();
    });

    jQuery("main").on("shown.bs.modal",".arabic #mainvideoModalAra",function(){
        var vid = document.getElementById("mainvideoar");
        vid.play();
    });

    jQuery("main").on("hide.bs.modal",".arabic #mainvideoModalAra",function(){
        var vid = document.getElementById("mainvideoar");
        vid.load();
    });

    /* MENU CLICK, MENU ACTIVE STATE, BOX SLIDE FUNCTION - SECTION 6 */

    jQuery("main").on("click",".menu-in", function(e){
        e.stopPropagation();
        if (jQuery(this).hasClass("open-box")) {
            jQuery('.icon-box').addClass('display');
            jQuery('.data-container.gallery').hide();
            jQuery('.data-container.services').show();
        }
        if (jQuery(this).hasClass("open-popup")) {
            jQuery('.data-container.services').hide();
            jQuery('.data-container.gallery').show();
        }
        var button_index = jQuery(this).parent(".form-check").index(".menu-form .form-check");
        jQuery(this).addClass("active");
        jQuery("main").find(".menu-in").not(this).removeClass("active");
        jQuery("main").find(".data-box-in:eq(" + button_index + ")").css({
            "visibility": "visible",
            "height": "auto",
            "overflow": "visible"
        });
        jQuery("main").find(".data-box-in").not(".data-box-in:eq(" + button_index + ")").css({
            "visibility": "hidden",
            "height": "0",
            "overflow": "hidden"
        });
    });

    /* GALLERY POPUP FUNCTION - SECTION 6 */

    jQuery("main").on("click","[data-target='#galleryModal']", function(){
        jQuery('#galleryModal').modal();
    });

    jQuery("main").on("click","[data-target='#videoModal']", function(){
        jQuery('#videoModal').modal();
    });

    jQuery("main").on("shown.bs.modal","#videoModal",function(){
        var vid = document.querySelector("#videoModal video");
        vid.load();
        vid.play();
    });

    jQuery("main").on("hide.bs.modal","#videoModal",function(){
        var vid = document.querySelector("#videoModal video");
        vid.load();
    });

    jQuery("main").on("click",".close-gallery", function(){
        jQuery('.data-container.gallery').hide();
        jQuery('.data-container.services').show();
    });

    /* GALLERY LAYOUT FUNCTION - SECTION 6 */

    jQuery("main").on("click",".open-popup", function(){
        var $grid = jQuery('.grid').masonry({
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer',
            itemSelector: '.grid-item',
            percentPosition: true
        });
        $grid.imagesLoaded().progress( function() {
            $grid.masonry();
        });
    });

    /* ROUND BUTTON HOME POPUP FUNCTION - SECTION 6 */

    jQuery("main").on("click",".btn_home", function(e){
        e.stopPropagation();
        jQuery('.dd_home').fadeToggle();
    });

    jQuery("main").on("click", function(){
        jQuery('.dd_home').fadeOut();
    });

    /* ROUND BUTTON SHOW SELECTED FUNCTION - SECTION 6 */

    jQuery("main").on("click",".btn_selected", function(e){
        e.stopPropagation();
        jQuery('.dd_selected').fadeToggle();
    });

    jQuery("main").on("click",".dd_selected", function(e){
        e.stopPropagation();
    });

    jQuery("main").on("click", function(){
        jQuery('.dd_selected').fadeOut();
    });

    /* ROUND BUTTON EMAIL POPUP FUNCTION - SECTION 6 */

    jQuery("main").on("click",".english .btn_email", function(){
        jQuery('#formModal').modal({
            backdrop: false
        });
    });

    jQuery("main").on("click",".arabic .btn_email", function(){
        jQuery('#formModalAra').modal({
            backdrop: false
        });
    });

    //COMMON SIDE BUTTONS UNDER SHARE ICON

    jQuery("main").on("click",".dropdown.dd_home a.city-map",function(){
        console.log("logging citymap")
    });

    jQuery("main").on("click",".dropdown.dd_home a.reset",function(){
        console.log("logging reset");
    });

    jQuery("main").on("click",".btn_round.mail",function(){
        console.log(APP.selectedCities);
    });

    jQuery("main").on("click",".btn_round.briefcase",function(){
        console.log("logging case")
    });


    jQuery("main").on("click",".btn_round.plus",function(){
        let city = $('section').attr("data-name");
        let cityid = $('section').attr("data");
        let url = $('section').attr("data-url");
        let exist = false;
        APP.selectedCities.forEach(element => {
            if(element.id===cityid)
                exist = true;
        });
        if(exist === false){
            APP.selectedCities.push({
                name: city,
                id : cityid,
                url : url,
                no : APP.selectedCities.length+1
            })
        }
        const checkboxHeader = `
                        <div class="checkbox">
                            <label class="form-check-label" id="check0" for="check0"><span>Selected Cities</span></label>
                        </div>`;

        const checkbox = `
        ${APP.selectedCities.map(city =>
                        `
                        <div class="checkbox">
                            <input class="form-check-input" type="checkbox" checked value="${city.id}" id="check${city.no}">
                            <label class="form-check-label" for="check${city.id}"><span>${city.name}</span></label>
                        </div>
                        `
                        ).join('')}
                    `;
        APP.selectedCitiesHtml = checkboxHeader+checkbox;
        $('.dropdown.dd_selected').first().after().html('').append(APP.selectedCitiesHtml);
        $('.btn_round.briefcase .badge').html(APP.selectedCities.length);
        // $(".checkbox input").prop("checked",true)
    });



     jQuery("main").on("click",".sbmt", addForm);

    //  SUBMIT FORM DATA

    function addForm(){

        $('#msg-en').hide(); $('#msg-ar').hide();

        APP.selectedCities;
        selSection = $('section').attr("id");
        selLang = $('#'+selSection).attr("class").split(/\s+/)[0];
        name = $('#fname').val().trim();
        company = $('#company').val().trim();
        jobtitle = $('#jtitle').val().trim();
        email = $('#email').val().trim();

        if(email == ''){
            if(selLang == 'english'){
                $('#msg-en').html('Email is mandatory.').show();
            }else{
                $('#msg-ar').html('البريد الإلكتروني إلزامي').show();
            }
            return;
        }

        //english arabic
         $.ajax({
            type: "POST",
            url: '/saveFormData',
            data : { selectedCities:APP.selectedCities, selLang:selLang, name:name, company:company,jobtitle:jobtitle, email:email  },
            dataType: "json",
            processdata: true,

            success: function(re) {
                if(selLang == 'english'){
                    $('#msg-en').html('Thank you for contacting Modon.').show();
                }else{
                    $('#msg-ar').html('شكرا على التواصل').show();
                }
                setTimeout(function(){ window.location.reload(true) }, 2000);
            },

            error: function() {

            }
        });
    }



});
