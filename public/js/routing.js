var APP = {}

jQuery(document).ready(function() {

    /* NAVIGATION ACROSS THE APP */

    //section 1 to section 2

    jQuery("main").on("click",".logo-center", function(){
        jQuery("main").load("pages/section2",function(e){
          section2('section2')
        });
    });

    /* HOME BUTTON RIYADH MAP LINK - REDIRECTION TO SECTION 4 */

    // english

    jQuery("main").on("click",".english .main-map", function(){
        jQuery("main").load("pages/section4 .english",function(e){
          section4('section4');
        });
    });

    // arabic

    jQuery("main").on("click",".arabic .main-map", function(){
        jQuery("main").load("pages/section4 .arabic",function(e){
          section4('section4');
        });
    });

    /* PLAY VIDEO ON SECTION 3 - REDIRECTION TO SECTION 4 */

    // english

    jQuery("main").on("click",".play-btn1", function(){
      jQuery('.video-poster').fadeOut();
      jQuery('.video-controls').fadeOut();
      var play_vdo1 = document.getElementById("modon-vdo1");
      play_vdo1.play();
      jQuery(play_vdo1).on('ended',function(){
          jQuery("main").load("pages/section4 .english",function(e){
            section4('section4');
          });
      });
    });

    //arabic

    jQuery("main").on("click",".play-btn2", function(){
        jQuery('.video-poster').fadeOut();
        jQuery('.video-controls').fadeOut();
        var play_vdo2 = document.getElementById("modon-vdo2");
        play_vdo2.play();
        jQuery(play_vdo2).on('ended',function(){
            jQuery("main").load("pages/section4 .arabic");
        });
    });

    /* SKIP BUTTON ON SECTION 3 - REDIRECTION TO SECTION 4 */

    // english

    jQuery("main").on("click",".skip-btn1", function(){
        jQuery("main").load("pages/section4 .english",function(e){
          section4('section4');
        });
    });

    //arabic

    jQuery("main").on("click",".skip-btn2", function(){
        jQuery("main").load("pages/section4 .arabic",function(e){
          section4('section4');
        });
    });

    /* SKIP BUTTON APPEAR ON SECTION 3 VIDEO */

    // english

    jQuery("main").on("click","#modon-vdo1", function(){
        jQuery(".english .video-controls.playback").fadeIn(800,function(){
            setTimeout(function(){
                jQuery(".english .video-controls.playback").fadeOut(800);
            },2000)
        });
    })

    //arabic

    jQuery("main").on("click","#modon-vdo2", function(){
        jQuery(".arabic .video-controls.playback").fadeIn(800,function(){
            setTimeout(function(){
                jQuery(".arabic .video-controls.playback").fadeOut(800);
            },2000)
        });
    })

    /* LANGUAGE CHANGE BUTTON ON EACH SECTION */

    // english to arabic

    jQuery("main").on("click","#section3.english .btn2", function(){
        jQuery("main").load("pages/section3 .arabic",function(e){
          section3('section3');
        });
    });

    jQuery("main").on("click","#section4.english .btn2", function(){
        jQuery("main").load("pages/section4 .arabic",function(e){
          section4('section4');
        });
    });

    jQuery("main").on("click","#section7.english .btn2", function(){
        jQuery("main").load("pages/section7 .arabic",function(e){
          section7('section7');
        });
    });

    jQuery("main").on("click","#section8.english .btn2", function(){
        jQuery("main").load("pages/section8 .arabic",function(e){
          section8('section8');
        });
    });

    jQuery("main").on("click","#section9.english .btn2", function(){
        jQuery("main").load("pages/section9 .arabic",function(e){
          
        });
    });

    // arabic to english

    jQuery("main").on("click","#section3.arabic .btn1", function(){
        jQuery("main").load("pages/section3 .english",function(e){
          section3('section3');
        });
    });

    jQuery("main").on("click","#section4.arabic .btn1", function(){
        jQuery("main").load("pages/section4 .english",function(e){
          section4('section4');
        });
    });

    jQuery("main").on("click","#section7.arabic .btn1", function(){
        jQuery("main").load("pages/section7 .english",function(e){
          
        });
    });

    jQuery("main").on("click","#section8.arabic .btn1", function(){
        jQuery("main").load("pages/section8 .english",function(e){
          
        });
    });

    jQuery("main").on("click","#section9.arabic .btn1", function(){
        jQuery("main").load("pages/section9 .english",function(e){
            
        });
    });

    //section 4 to section 6

    

    jQuery("main").on("click","#alraidahPopup.card .card-body .card-link", function(){
      let lang = $('#section4').attr("class").split(/\s+/)[0];
      jQuery("main").load("pages/section9"+" ."+lang,function(e){
            section9('section9');
      });
  });

    jQuery("main").on("click","#wadiPopup.card .card-body .card-link", function(){
      let lang = $('#section4').attr("class").split(/\s+/)[0];
      jQuery("main").load("pages/section8"+" ."+lang,function(e){
            section8('section8');
      });
    });

    //section 4 to section 7

    jQuery("main").on("click","#riyadhPopup.card .card-body .card-link", function(){
        let lang = $('#section4').attr("class").split(/\s+/)[0];
        jQuery("main").load("pages/section7"+" ."+lang,function(e){
            section7('section7');
        });
    });

});
