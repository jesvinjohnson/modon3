var APP = {}

jQuery(document).ready(function() {

    /* NAVIGATION ACROSS THE APP */

    //Load iPad and Desktop together 
    $.ajax({
      type: "GET",
      url: '/onLoad',
      dataType: "json",
      processdata: true,
      success: function(re) {
      //reloading Desktop
      }
    });

     /* NAVIGATION ACROSS THE APP */

    //section 1 to section 2

  jQuery("main").on("click",".logo-center", function(){
      jQuery("main").load("pages-ipad/section2",function(e){
        section2('section2')
      });
  });

  /* HOME BUTTON RIYADH MAP LINK - REDIRECTION TO SECTION 4 */

  // english

  jQuery("main").on("click",".english .main-map", function(){
      jQuery("main").load("pages-ipad/section4?btn=en-mainmap .english",function(e){
        section4('section4');
      });
  });

  // arabic

  jQuery("main").on("click",".arabic .main-map", function(){
      jQuery("main").load("pages-ipad/section4?btn=ar-mainmap .arabic",function(e){
        section4('section4');
      });
  });

  /* PLAY VIDEO ON SECTION 3 - REDIRECTION TO SECTION 4 */

  // english

  jQuery("main").on("click",".play-btn1", function(){

     $.ajax({
        type: "GET",
        url: '/videobtn?id=play-btn1',
        dataType: "json",
        processdata: true,

        success: function(re) {
          
            var play_vdo1 = document.getElementById("modon-vdo1");
            play_vdo1.play();
            jQuery(play_vdo1).on('ended',function(){
              jQuery("main").load("pages-ipad/section4?btn=skipbtn1 .english",function(e){
                section4('section4');
              });
          });

        },

        error: function() {

        }
      }); 

 
  });

  //arabic

  jQuery("main").on("click",".play-btn2", function(){
      

      $.ajax({
        type: "GET",
        url: '/videobtn?id=play-btn2',
        dataType: "json",
        processdata: true,

        success: function(re) {
          
            var play_vdo2 = document.getElementById("modon-vdo2");
            play_vdo2.play();
          jQuery(play_vdo2).on('ended',function(){
              jQuery("main").load("pages-ipad/section4?btn=skipbtn2 .arabic");
          });

        },

        error: function() {

        }
      }); 


  });

  /* SKIP BUTTON ON SECTION 3 - REDIRECTION TO SECTION 4 */

  // english

  jQuery("main").on("click",".skip-btn1", function(){
      jQuery("main").load("pages-ipad/section4?btn=skipbtn1 .english",function(e){
        section4('section4');
      });
  });

  //arabic

  jQuery("main").on("click",".skip-btn2", function(){
      jQuery("main").load("pages-ipad/section4?btn=skipbtn2 .arabic",function(e){
        section4('section4');
      });
  });

  /* SKIP BUTTON APPEAR ON SECTION 3 VIDEO */

  // english

  jQuery("main").on("click","#modon-vdo1", function(){
      jQuery(".english .video-controls.playback").fadeIn(800,function(){
          setTimeout(function(){
              jQuery(".english .video-controls.playback").fadeOut(800);
          },2000)
      });
  })

  //arabic

  jQuery("main").on("click","#modon-vdo2", function(){
      jQuery(".arabic .video-controls.playback").fadeIn(800,function(){
          setTimeout(function(){
              jQuery(".arabic .video-controls.playback").fadeOut(800);
          },2000)
      });
  })

  /* LANGUAGE CHANGE BUTTON ON EACH SECTION */

  // english to arabic

  jQuery("main").on("click","#section3.english .btn2", function(){
      jQuery("main").load("pages-ipad/section3?lang=sec3-ar .arabic",function(e){
        section3('section3');
      });
  });

  jQuery("main").on("click","#section4.english .btn2", function(){
      jQuery("main").load("pages-ipad/section4?btn=ar-btn2 .arabic",function(e){
        section4('section4');
      });
  });

  jQuery("main").on("click","#section6.english .btn2", function(){
      cityID= $('#section6').attr('data');
      jQuery("main").load("pages-ipad/section6?type=sec6-btn2&&id="+cityID+" .arabic",function(e){
        section6('section6');
      });
  });

  jQuery("main").on("click","#section7.english .btn2", function(){
      jQuery("main").load("pages-ipad/section7?type=sec7-btn2 .arabic",function(e){
        section7('section7');
      });
  });

  jQuery("main").on("click","#section8.english .btn2", function(){
      jQuery("main").load("pages-ipad/section8?type=sec8-btn2 .arabic",function(e){
        section8('section8');
      });
  });

  jQuery("main").on("click","#section9.english .btn2", function(){
      jQuery("main").load("pages-ipad/section9?type=sec9-btn2 .arabic",function(e){
        section9('section9');
      });
  });

  // arabic to english

  jQuery("main").on("click","#section3.arabic .btn1", function(){
      jQuery("main").load("pages-ipad/section3?lang=sec3-en .english",function(e){
        section3('section3');
      });
  });

  jQuery("main").on("click","#section4.arabic .btn1", function(){
      jQuery("main").load("pages-ipad/section4?btn=en-btn1 .english",function(e){
        section4('section4');
      });
  });

  jQuery("main").on("click","#section6.arabic .btn1", function(){
      cityID= $('#section6').attr('data');
      jQuery("main").load("pages-ipad/section6?type=sec6-btn1&&id="+cityID+" .english",function(e){
        section6('section6');
      });
  });

  jQuery("main").on("click","#section7.arabic .btn1", function(){
      jQuery("main").load("pages-ipad/section7?type=sec7-btn1 .english",function(e){
        section7('section7');
      });
  });

  jQuery("main").on("click","#section8.arabic .btn1", function(){
      jQuery("main").load("pages-ipad/section8?type=sec8-btn1 .english",function(e){
        section8('section8');
      });
  });

  jQuery("main").on("click","#section9.arabic .btn1", function(){
      jQuery("main").load("pages-ipad/section9?type=sec9-btn1 .english",function(e){
        section9('section9');
      });
  });

  //section 4 to section 6

  jQuery("main").on("click","#mapPopup.card .card-body .card-link", function(){
    let lang = $('#section4').attr("class").split(/\s+/)[0];
    let cityID = $(this).attr("id");
    jQuery("main").load("pages-ipad/section6?type=sec6-more&&id="+cityID+" ."+lang,function(e){
      section6('section6');
    });
  });

  jQuery("main").on("click","#alraidahPopup.card .card-body .card-link", function(){

    jQuery("main").load("pages-ipad/section9?type=sec9-more .english",function(e){
        section9('section9')
    });
});

  jQuery("main").on("click","#wadiPopup.card .card-body .card-link", function(){
    jQuery("main").load("pages-ipad/section8?type=sec8-more .english",function(e){
        section8('section8')
    });
  });

  //section 4 to section 7

  jQuery("main").on("click","#riyadhPopup.card .card-body .card-link", function(){
      jQuery("main").load("pages-ipad/section7?type=sec7-more .english",function(e){
        section7('section7')
      });
  });



    //ONLOAD CODE FOR EACH SECTION

    function section2(section) {

      /* SECTION 2 ANIMATION */

      console.log(section);
      let english = document.getElementById('eng_vdo')
      var animation =  lottie.loadAnimation({
          container: english,
          renderer: 'svg',
          loop: true,
          autoplay: true,
          path: './svg/english_video/data.json',
      });
      
      /* SECTION 2 REDIRECTION TO SECTION 3 */

      animation.addEventListener('DOMLoaded', function(){

          // english

          document.querySelector('.eng').addEventListener('click',function(){
              jQuery("main").load("pages-ipad/section3?lang=sec3-en .english",function(e){
                  section3('section3');
              });
          });

          //arabic

          document.querySelector('.arabic').addEventListener('click',function(){
              jQuery("main").load("pages-ipad/section3?lang=sec3-ar .arabic",function(e){
                  section3('section3');
              });
          });
      });
  }



    function section3(section) {
      console.log(section)
    }

    //Factory function returning Lottie Objects for map regions   
    function getLottie(state){
        
        let svgContainer = document.getElementById('forStateSvg');
        let path2anim = './svg/states/';
        let lottieObj = {
          container: svgContainer,
          path: path2anim+state+'.json',
          renderer: 'svg',
          loop: false,
          autoplay: false,
        };

        return lottieObj;

    }
    
    //Fetch Data from API for popups on cites
    function getIndustrialData(industrySelector) {

      $.ajax({
          type: "POST",
          url: '/fetchIndustrialData',
          data : { industryData : industrySelector },
          dataType: "json",
          processdata: true,

          success: function(re) {
              let lang = $('#section4').attr("class").split(/\s+/)[0];
              let forPopup;
              if(lang=="english"){
                  forPopup = re.data;
              }else{
                  forPopup = re.data2;
              }
              
              $('#mapPopup').html('').html(forPopup)
              $('#forSvg #mapPopup').fadeIn();
              $('#forSvg #mapPopup').css("z-index","2");
          },

          error: function() {

          }
      });
  }    

    function fireSocket(industrySelector) {

      $.ajax({
          type: "get",
          url: '/fireSocket?sel='+industrySelector,
          dataType: "json",
          processdata: true,

          success: function(re) {
          },

          error: function() {

          }
      });
  }


    function section4(section) {

      sharePopupOpen()


      $('.dropdown.dd_selected').first().after().html('').append(APP.selectedCitiesHtml);
      $('#section4 .btn_round.briefcase .badge').html(APP.selectedCities.length);
      
      //Animations for each map region based on language selected
      
      let lang = $('#section4').attr("class").split(/\s+/)[0];
      if(lang=="english"){
        var riyadhAnim = lottie.loadAnimation(getLottie('riyadh'));
        var buraidahAnim = lottie.loadAnimation(getLottie('buraidah'));
        var medinaAnim = lottie.loadAnimation(getLottie('medina'));
        var hailAnim = lottie.loadAnimation(getLottie('hail'));
        var ararAnim = lottie.loadAnimation(getLottie('arar'));
        var skakaAnim = lottie.loadAnimation(getLottie('skaka'));
        var tabukAnim = lottie.loadAnimation(getLottie('tabuk'));
        var makkahAnim = lottie.loadAnimation(getLottie('makkah'));
        var albahaAnim = lottie.loadAnimation(getLottie('al_baha'));
        var abhaAnim = lottie.loadAnimation(getLottie('abha'));
        var jazanAnim = lottie.loadAnimation(getLottie('jazan'));
        var najranAnim = lottie.loadAnimation(getLottie('najran'));
        var dammanAnim = lottie.loadAnimation(getLottie('damman'));
      }else {

        var riyadhAnim = lottie.loadAnimation(getLottie('riyadhAr'));
        var buraidahAnim = lottie.loadAnimation(getLottie('buraidahAr'));
        var hailAnim = lottie.loadAnimation(getLottie('hailAr'));
        var medinaAnim = lottie.loadAnimation(getLottie('medinaAr'));
        var ararAnim = lottie.loadAnimation(getLottie('ararAr'));
        var skakaAnim = lottie.loadAnimation(getLottie('skakaAr'));
        var tabukAnim = lottie.loadAnimation(getLottie('tabukAr'));
        var makkahAnim = lottie.loadAnimation(getLottie('makkahAr'));
        var albahaAnim = lottie.loadAnimation(getLottie('al_bahaAr'));
        var abhaAnim = lottie.loadAnimation(getLottie('abhaAr'));
        var jazanAnim = lottie.loadAnimation(getLottie('jazanAr'));
        var najranAnim = lottie.loadAnimation(getLottie('najranAr'));
        var dammanAnim = lottie.loadAnimation(getLottie('dammanAr'));
      }


      //Functionality after map region animation completes

      riyadhAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      buraidahAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      hailAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      medinaAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      ararAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      skakaAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      tabukAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      makkahAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      albahaAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      abhaAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      jazanAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      najranAnim.onComplete = function(e){
          sideBarFadeIn();
      }

      dammanAnim.onComplete = function(e){
          sideBarFadeIn();
      }

    //ASSIGNING CITY TYPES(Industrial,Technology,ModonOasis) and CLICK functionality to cities inside Map Regions

      riyadhAnim.addEventListener("DOMLoaded",function(){

          //ASSIGNING CITY TYPES

          $('.Riyadh1_1, .AlZulfi_21, .Sudayer_16, .Shaqra_32, .Dhurma_37, .Riyadh3_26, .AlKharj_17, .Riyadh2_2').addClass("industry");
          $('.RiyadhTechnology,.AlRaidah').addClass("technology");

          $('.Riyadh2_2, .Sudayer_16, .AlZulfi_21, .Shaqra_32, .Riyadh3_26, .Dhurma_37, .AlKharj_17, .Riyadh1_1').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          });

          $('.RiyadhTechnology').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              $('#forSvg #riyadhPopup').fadeIn();
              $('#forSvg #riyadhPopup').css("z-index","2");
              e.stopPropagation();
          });

          $('.AlRaidah').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              $('#forSvg #alraidahPopup').fadeIn();
              $('#forSvg #alraidahPopup').css("z-index","2");
              e.stopPropagation();
          });
      });

      buraidahAnim.addEventListener("DOMLoaded",function(){

          $('.AlQassim_7, .AlQassim2_25').addClass("industry");
          $('.AlQassimOsais_44').addClass("modern");

          $('.AlQassimOsais_44, .AlQassim_7, .AlQassim2_25').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              fireSocket(temp)
              console.log(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      });

      hailAnim.addEventListener("DOMLoaded",function(){
          $('.Hail_13').addClass("industry");

          $('.Hail_13').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      medinaAnim.addEventListener("DOMLoaded",function(){

          $('.AlMadinaAlMunawwara_9').addClass("industry");
          $('.YanbueOsais_42').addClass("modern");

          $('.AlMadinaAlMunawwara_9, .YanbueOsais_42').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      ararAnim.addEventListener("DOMLoaded",function(){

          $('.Arar_19, .waadalshamal_48').addClass("industry");
          $('.Arar_19, .waadalshamal_48').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      skakaAnim.addEventListener("DOMLoaded",function(){
          $('.skaka12').addClass("industry");
          $('.AlJoufOsais_11').addClass("modern");
          //NO IDS in SVG
          $('.AlJoufOsais_11').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })

      })

      tabukAnim.addEventListener("DOMLoaded",function(){

          $('.Tabuk_12').addClass("industry");

          $('.Tabuk_12').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      makkahAnim.addEventListener("DOMLoaded",function(){

          $('.Rabigh_39, .Jeddah1_3, .Jeddah2_15, .Jeddah3_28, .MakkahAlMukarrama_6').addClass("industry");
          $('.WadiMakkah').addClass("technology");
          $('.JeddahOsais_29').addClass("modern");

          $('.Rabigh_39, .Jeddah1_3, .Jeddah2_15, .Jeddah3_28, .MakkahAlMukarrama_6, .JeddahOsais_29').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })

          $('.WadiMakkah').click(function(e){
              console.log($(this).attr("class").split(/\s+/)[0])
              var temp = $(this).attr("class").split(/\s+/)[0];
              fireSocket(temp)
              $('#forSvg #wadiPopup').fadeIn();
              $('#forSvg #wadiPopup').css("z-index","2");
              e.stopPropagation();
          });
      })

      albahaAnim.addEventListener("DOMLoaded",function(){
          $('.Albaha_27').addClass("industry");

          $('.Albaha_27').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      abhaAnim.addEventListener("DOMLoaded",function(){

          $('.Assir_10').addClass("industry");

          $('.Assir_10').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      jazanAnim.addEventListener("DOMLoaded",function(){

          $('.Jazan_18').addClass("industry");

          $('.Jazan_18').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      najranAnim.addEventListener("DOMLoaded",function(){

          $('.Najran_14').addClass("industry");

          $('.Najran_14').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          })
      })

      dammanAnim.addEventListener("DOMLoaded",function(){

          $('.Alahsa_36, .Dammam_5, .Dammam1_4, .Dammam3_30, .Ahsa1_8, .HafrAlbatin_35').addClass("industry");
          $('.AhsaOsais_38').addClass("modern");


          $('.Alahsa_36, .Dammam_5, .Dammam1_4, .Dammam3_30, .Ahsa1_8, .HafrAlbatin_35, .AhsaOsais_38').click(function(e){
              var temp = $(this).attr("class").split(/\s+/)[0];
              console.log(temp)
              fireSocket(temp)
              getIndustrialData(temp)
              e.stopPropagation();
          });
      })

      // CITY TYPE FILTERS(Industy,Technology,ModonOasis)

      $('.checkbox input').on("change",function(){
          if($("#check1").is(':checked')){
              checkuncheck('check1');
              $('.industry').fadeIn(500);
          }else{
              checkuncheck('uncheck1');
              $('.industry').fadeOut(500);
          }
          if($("#check2").is(':checked')){
              checkuncheck('check2');
              $('.technology').fadeIn(500);
          }else{
              checkuncheck('uncheck2');
              $('.technology').fadeOut(500);
          }
          if($("#check3").is(':checked')){
              checkuncheck('check3');
              $('.modern').fadeIn(500);
          }else{
              checkuncheck('uncheck3');
              $('.modern').fadeOut(500);
          }
          if(($("#check1").is(':checked')||$("#check2").is(':checked')||$("#check3").is(':checked'))==false){
              $('.modern, .industry, .technology').fadeOut(100);
          }
      })

      function checkuncheck(argument) {
          url = argument
          $.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            processdata: true,

            success: function(re) {
            },

            error: function() {

            }
          });
      }

      //MAP REGION ANIMATIONS ON SAUDI MAP CLICK FUNCTIONALITY


      $('#Riyadh,[data-name=" Riyadh"],#_Riyadh').on("click",function(){
          stateAnimate('.Riyadh',riyadhAnim);
           if(lang== 'english')
              $('#section4 .map-head.city').html("Al Riyadh")
          else
              $('#section4 .map-head.city').html("الرياض")
      })

      $('#Buraidah,[data-name="buraidah"],#buraidah-2').on("click",function(){
          stateAnimate('.Buraidah',buraidahAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Qassim")
          else
              $('#section4 .map-head.city').html("القصيم")
      })

      $('#Hali,[data-name=" Hail"],#_Hail').on("click",function(){
          stateAnimate('.Hail',hailAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Hail")
          else
              $('#section4 .map-head.city').html("حائل")
      })

      $('#Medina,[data-name=" Medina"],#_Medina').on("click",function(){
          stateAnimate('.Medina',medinaAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Madinah")
          else
              $('#section4 .map-head.city').html("المدينة المنورة")
      })

      $('#Arar,[data-name=" Arar"],#_Arar').on("click",function(){
          stateAnimate('.Arar',ararAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Northern")
          else
              $('#section4 .map-head.city').html("الحدود الشمالية")
      })

      $('#Skaka,[data-name=" Skaka"],#_Skaka').on("click",function(){
          stateAnimate('.Skaka1',skakaAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Al Jawf")
          else
              $('#section4 .map-head.city').html("الجوف")
      })

      $('#Tabuk,[data-name=" Tabuk"],#_Tabuk').on("click",function(){
          stateAnimate('.Tabuk1',tabukAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Tabuk")
          else
              $('#section4 .map-head.city').html("تبوك")
      })

      $('#Makkah,[data-name=" Makkah"],#makkah-2').on("click",function(){
          stateAnimate('.Makkah1',makkahAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Makkah")
          else
              $('#section4 .map-head.city').html("مكة المكرمة")
      })

      $('#Al_Baha,[data-name=" Al_baha"],#_Al_Baha').on("click",function(){
          stateAnimate('.Al',albahaAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Baha")
          else
              $('#section4 .map-head.city').html("الباحة")
      })

      $('#Abha,[data-name="abha-2"],#abha-2').on("click",function(){
          stateAnimate('.Abha',abhaAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Asir")
          else
              $('#section4 .map-head.city').html("عسير")
      })

      $('#Jazan,[data-name=" Jazan"],#_Jazan').on("click",function(){
          stateAnimate('.Jazan1',jazanAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Jazan")
          else
              $('#section4 .map-head.city').html("جازان")
      })

      $('#Najran,[data-name=" Najran"],#Najran-2').on("click",function(){
          stateAnimate('.Najran1',najranAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Najran")
          else
              $('#section4 .map-head.city').html("نجران")
      })

      $('#Dammam,[data-name=" Damman"],#dammam-2').on("click",function(){
          stateAnimate('.Damman1a',dammanAnim);
          if(lang == 'english')
              $('#section4 .map-head.city').html("Eastern");
          else
              $('#section4 .map-head.city').html("الشرقية");
      })

  }



    function section5(section) {
      console.log(section)
    }

    //SHARE BUTTON MESSAGES TO DESKTOP 

    function sharePopupOpen(){
      
      $('.fa-share-alt').parent().click(function(){ 
        $.ajax({
          type: "GET",
          url: '/sharePopupOpen?type=sharebtn',
          dataType: "json",
          processdata: true,
          success: function(re) {

          },
        });
      }) 


      $('.share-holder .btn_round.mail').click(function(){
        $.ajax({
          type: "GET",
          url: '/sharePopupOpen?type=mailbtn',
          dataType: "json",
          processdata: true,
          success: function(re) {

          },
        });
      })

      
    }


    function section6(section) {
    sharePopupOpen()
    $('.cs ul.list li').first().hide();
    $('.is ul.list li').first().hide();
    $('.rs ul.list li').first().hide();
    
    //SIDEBAR FUNCTIONALITY MESSAGES(TargetedIndustry, Infrastructure . . .) 

    document.getElementById('tar_ind').addEventListener('click',function(){ 
      $.ajax({
        type: "GET",
        url: '/section6LeftNav?type=tar_ind',
        dataType: "json",
        processdata: true,
        success: function(re) {
        },
      });
    })
    document.getElementById('inf_ser').addEventListener('click',function(){ 
      $.ajax({
        type: "GET",
        url: '/section6LeftNav?type=inf_ser',
        dataType: "json",
        processdata: true,
        success: function(re) {
        },
      });
    })
    document.getElementById('com_ser').addEventListener('click',function(){ 
      $.ajax({
        type: "GET",
        url: '/section6LeftNav?type=com_ser',
        dataType: "json",
        processdata: true,
        success: function(re) {
        },
      });
    })
    document.getElementById('res_ser').addEventListener('click',function(){ 
      $.ajax({
        type: "GET",
        url: '/section6LeftNav?type=res_ser',
        dataType: "json",
        processdata: true,
        success: function(re) {
        },
      });
    })
    document.getElementById('gal').addEventListener('click',function(){ 
      $.ajax({
        type: "GET",
        url: '/section6LeftNav?type=gal',
        dataType: "json",
        processdata: true,
        success: function(re) {
        },
      });
    })

      // BAR CHART CODE
      var dataGraph = JSON.parse($('#targtdInd').val())
      var data = {
          labels: [],
          series: []
      };
      data.labels = dataGraph.labels
      data.series.push(dataGraph.series)
      console.log(data)
      if(dataGraph.series.length==0){
        $('.icon-box').next().next().hide()
        }
      var options = {
          fullWidth: true,
          seriesBarDistance: 15,
          axisX: {
              offset: 90
          },
          axisY: {
              onlyInteger: true
          }
      };
      new Chartist.Bar('.ct-chart', data, options);
      APP.makeCheckbox();

      //Gallery modal functionality

      $('[data-target="#galleryModal"]').click(function(e){
          let img = $(this).find('img').attr("src");
          let id = $(this).attr("id");
          console.log('videoModal'+id)
          $('#galleryModal').find('img').attr("src",img);

        $.ajax({
          type: "POST",
          url: '/openPopImgVideo',
          data : { datasrc: id},
          dataType: "json",
          processdata: true,

          success: function(re) {
  
          },

          error: function() {

          }
        }); 


      })

      
      $('[data-target="#videoModal"]').click(function(e){
          let video = $(this).find('source').attr("src");
          let id = $(this).attr("id");
          console.log('videoModal'+id)
          $('#videoModal').find('source').attr("src",video);
          $.ajax({
            type: "POST",
            url: '/openPopImgVideo',
            data : { datasrc: id},
            dataType: "json",
            processdata: true,

            success: function(re) {

            },

            error: function() {

            }
          });
      })


  }

  // Gallery VIDEO Message to Desktop
  function openImgVideoModal() {

    $('[data-target="#galleryModal"]').click(function(e){
          let img = $(this).find('img').attr("src");
          let id = $(this).attr("id");
          console.log('videoModal'+id)
          $('#galleryModal').find('img').attr("src",img);

        $.ajax({
          type: "POST",
          url: '/openPopImgVideo',
          data : { datasrc: id},
          dataType: "json",
          processdata: true,

          success: function(re) {

          },

          error: function() {

          }
        }); 


      })

      $('[data-target="#videoModal"]').click(function(e){
          let video = $(this).find('source').attr("src");
          let id = $(this).attr("id");
          console.log('videoModal'+id)
          $('#videoModal').find('source').attr("src",video);
          $.ajax({
            type: "POST",
            url: '/openPopImgVideo',
            data : { datasrc: id},
            dataType: "json",
            processdata: true,

            success: function(re) {

            },

            error: function() {

            }
          });
      })
    // body...
  }

    // TECH ZONE PAGES
    
    function section7(section) {
        APP.makeCheckbox()
        sharePopupOpen()
        openImgVideoModal()
        console.log(section)
        document.getElementById('proj').addEventListener("click",function(){
            $.ajax({
                type: "GET",
                url: '/techZoneLeftNav?type=proj',
                dataType: "json",
                processdata: true,
                success: function(re) {
                },
              });
        })
        document.getElementById('gal').addEventListener("click",function(){
            $.ajax({
                type: "GET",
                url: '/techZoneLeftNav?type=galt',
                dataType: "json",
                processdata: true,
                success: function(re) {
                },
              });
        })
    }

    function section8(section) {
      APP.makeCheckbox()
      sharePopupOpen()
      openImgVideoModal()
      console.log(section)
      document.getElementById('proj').addEventListener("click",function(){
        $.ajax({
            type: "GET",
            url: '/techZoneLeftNav?type=proj',
            dataType: "json",
            processdata: true,
            success: function(re) {
            },
          });
    })
    document.getElementById('gal').addEventListener("click",function(){
        $.ajax({
            type: "GET",
            url: '/techZoneLeftNav?type=galt',
            dataType: "json",
            processdata: true,
            success: function(re) {
            },
          });
    })
    }

    function section9(section) {
      APP.makeCheckbox()
      sharePopupOpen()
      openImgVideoModal()
      console.log(section)
      document.getElementById('proj').addEventListener("click",function(){
        $.ajax({
            type: "GET",
            url: '/techZoneLeftNav?type=proj',
            dataType: "json",
            processdata: true,
            success: function(re) {
            },
          });
    })
    document.getElementById('gal').addEventListener("click",function(){
        $.ajax({
            type: "GET",
            url: '/techZoneLeftNav?type=galt',
            dataType: "json",
            processdata: true,
            success: function(re) {
            },
          });
    })
    }

    //COMMON SIDE BUTTONS FOR LOADING

    //reset button

    jQuery("main").on("click",".reset",function(){
        location.reload();
    });

    // ANIMATION FUNCTIONS

    function sideBarFadeIn(){
      $('nav').animate({"opacity":"1","z-index":"2"},200);
    }

    function stateAnimate(state,stateAnim){

      $.ajax({
        type: "POST",
        url: '/stateAnimate',
        data : { stname:state },
        dataType: "json",
        processdata: true,

        success: function(re) {

          jQuery('.map-head.region').hide();
          jQuery('.map-head.city').fadeIn();
          $('#forSvg #saudi').fadeOut(500);
          console.log("showing "+state)
          $(state).parent().parent().show();
          stateAnim.goToAndPlay(0,true);

        },

        error: function() {

        }
      });

    }

});
